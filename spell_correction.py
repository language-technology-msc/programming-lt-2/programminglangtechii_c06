import nltk

# user writes something
mistake = "ligting"

# we have a set of correct words
correct_words = ['apple', 'bag', 'drawing', 'listing', 'linking', 'living', 'lighting', 'orange', 'walking', 'zoo']

results_as_tuples = []
for word in correct_words:
    ed = nltk.edit_distance(mistake, word)
    print(word, ed)
    # create a tuple (word, edit distance)
    word_distance_tuple = (word, ed)
    # append the tuple in a list
    results_as_tuples.append(word_distance_tuple)

# function that sorts a list of tuples
# using the second element of the tuple
def sort_tuples(results_as_tuples):
    # key is set to sort using second element of
    # sublist lambda has been used
    results_as_tuples.sort(key=lambda x: x[1])
    return results_as_tuples

# Let's test the function
# Sort the the edit distance tuples and print ...
sorted = sort_tuples(results_as_tuples)
print(sorted)

best = sorted[0]
print("======================")
print("Correction:" + best[0])

