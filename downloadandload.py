import gensim.downloader
from gensim.models import Word2Vec
from utils import print_similar_words

# Show all available models in gensim-data
print(list(gensim.downloader.info()['models'].keys()))

# load a model
model_name = 'word2vec-google-news-300'
# Download (if needed) dataset/model and load it to memory
model = gensim.downloader.load(model_name)

# [=-------------------------------------------------] 2.9% 49.0/1662.8MB downloaded

# ['fasttext-wiki-news-subwords-300', 'conceptnet-numberbatch-17-06-300', 'word2vec-ruscorpora-300', 'word2vec-google-news-300', 'glove-wiki-gigaword-50', 'glove-wiki-gigaword-100', 'glove-wiki-gigaword-200', 'glove-wiki-gigaword-300', 'glove-twitter-25', 'glove-twitter-50', 'glove-twitter-100', 'glove-twitter-200', '__testing_word2vec-matrix-synopsis']
# [==================================================] 100.0% 1662.8/1662.8MB downloaded
# loaded word2vec-google-news-300

print('loaded ' + model_name)

print_similar_words(model, 'computer')
print_similar_words(model, 'robot')
print_similar_words(model, 'coronovarius')