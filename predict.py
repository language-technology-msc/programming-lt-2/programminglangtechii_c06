# import gensim
from gensim.models import Word2Vec
from utils import print_similar_words

model_path ="./model/word2vec.model"
# load a model
print('loading ' + model_path)
model = Word2Vec.load(model_path)
print('loaded ' + model_path)

# you can continue training
model.train([['one', 'more', 'sentence']], total_examples=1, epochs=1)


vector = model.wv['computer']
print(len(vector))
for i in range(len(vector)):
    print("{0:5}{1:100}".format(str(i), str(vector[i])))


print_similar_words(model.wv, 'computer')
print_similar_words(model.wv, 'robot')
print_similar_words(model.wv, 'coronovarius')

# similarities are symmetric
